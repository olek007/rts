// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "MyBlueprintFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class RTS_API UMyBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
	
public:
	UFUNCTION(BlueprintCallable, Category = Sound)
		static void SetSoundCalssVolume(USoundClass* targetSoundClass, float newVolume);

	UFUNCTION(BlueprintPure, Category = sound)
		static float GetSoundClassVolume(USoundClass* targetSoundClass);

};
