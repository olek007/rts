// Fill out your copyright notice in the Description page of Project Settings.

#include "RTS.h"
#include "MyBlueprintFunctionLibrary.h"

void UMyBlueprintFunctionLibrary::SetSoundCalssVolume(USoundClass* targetSoundClass, float newVolume)
{
	if (targetSoundClass)
	{
		targetSoundClass->Properties.Volume = newVolume;
	}
}

float UMyBlueprintFunctionLibrary::GetSoundClassVolume(USoundClass * targetSoundClass)
{
	if (targetSoundClass)
	{
		return targetSoundClass->Properties.Volume;
	}
	return 0.0f;
}


